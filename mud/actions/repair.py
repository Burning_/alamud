# -*- coding: utf-8 -*-
#==============================================================================

from .action import Action2
from mud.events import RepairEvent


class RepairAction(Action2):
    EVENT = RepairEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "repair"