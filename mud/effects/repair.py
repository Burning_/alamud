# -*- coding: utf-8 -*-
#==============================================================================

from .effect import Effect1
from mud.events import RepairEvent

class RepairEffect(Effect1):
    EVENT = RepairEvent
