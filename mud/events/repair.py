# -*- coding: utf-8 -*-
#==============================================================================

from .event import Event2

class RepairEvent(Event2):
    NAME = "repair"

    def perform(self):
        self.inform("repair")