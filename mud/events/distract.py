# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class DistractWithEvent(Event3):
    NAME = "distract-with"

    def perform(self):
        self.inform("distract-with")
